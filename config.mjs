// config.js

export const FILE_EXTENSION_REGEX = /\.[a-zA-Z0-9]+$/;
export const DEFAULT_CSV_FILE_SIZE = 1 * 1024 * 1024;
